This OpenERP module adds some features to Project Management.
1. Tabs consist "Components","Consumables","Tools" in Project
2. Two one2many fields in Task to log the component and consumable usages
3. Components and consumables can be counted to show the remaining in a project
