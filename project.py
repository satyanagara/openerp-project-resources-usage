from osv import osv
from osv import fields


class project_project(osv.osv):
 
    _name = 'project.project'
    _description = 'Project'
    _inherit = 'project.project'
 
    _columns = {
            'component_ids':fields.one2many('project.component','project_id','Components'),    
            'consumable_ids':fields.one2many('project.consumable','project_id','Consumables'),
            'tool_ids':fields.many2many('resource.resource','project_tool_rel','project_id','tool_id','Tools'), 
        }
    _defaults = {  
        'date_start': lambda *a: time.strftime('%Y-%m-%d')
        }
    
    def compute_remain_consu(self,cr,uid,ids,context=None):
        
        consum_ids = self.pool.get('project.consumable').search(cr,uid,[('project_id','=',ids[0])])
        for id in consum_ids:
            consum_obj = self.pool.get('project.consumable').browse(cr,uid,id)            
            #consum_uom_obj = self.pool.get('product.uom').browse(cr,uid,consum_obj.product_uom_id.id)
            
            task_ids = self.pool.get('project.task').search(cr,uid,[('project_id','=',ids[0]),('state','<>','draft')])
            
            
            for task_id in task_ids:
                usage_ids = self.pool.get('project.task.usage.consumable').search(cr,uid,[('consumable_id','=',id),('task_id','=',task_id)])
                usage_qty = 0.00
                for usage_id in usage_ids:
                    usage_obj = self.pool.get('project.task.usage.consumable').browse(cr,uid,usage_id)
                    ratio = consum_obj.product_uom_id.factor/usage_obj.uom_id.factor
                    usage_qty += usage_obj.quantity * ratio
                    task = self.pool.get('project.task').browse(cr,uid,task_id)
                    print """CONID: %s, CON: %s, TASK: %s, USAGE:%s""" % (consum_obj.id,consum_obj.name,task.name,usage_qty)
                    sql = """UPDATE project_consumable SET remain_qty = (quantity - %s) WHERE id = %s AND project_id = %s""" %(usage_qty, id,ids[0]) 
                    cr.execute(sql)
                    print "SQL: ", sql
        
        return True
    
    def compute_remain_component(self,cr,uid,ids,context=None):
        
        component_ids = self.pool.get('project.component').search(cr,uid,[('project_id','=',ids[0])])
        for id in component_ids:
            component_obj = self.pool.get('project.component').browse(cr,uid,id)            
            #consum_uom_obj = self.pool.get('product.uom').browse(cr,uid,consum_obj.product_uom_id.id)
            
            task_ids = self.pool.get('project.task').search(cr,uid,[('project_id','=',ids[0]),('state','<>','draft')])
            
            
            for task_id in task_ids:
                usage_ids = self.pool.get('project.task.usage.component').search(cr,uid,[('component_id','=',id),('task_id','=',task_id)])
                usage_qty = 0.00
                for usage_id in usage_ids:
                    usage_obj = self.pool.get('project.task.usage.component').browse(cr,uid,usage_id)
                    ratio = component_obj.product_uom_id.factor/usage_obj.uom_id.factor
                    usage_qty += usage_obj.quantity * ratio
                    task = self.pool.get('project.task').browse(cr,uid,task_id)
                    print """CONID: %s, CON: %s, TASK: %s, USAGE:%s""" % (component_obj.id,component_obj.name,task.name,usage_qty)
                    sql = """UPDATE project_component SET remain_qty = (quantity - %s) WHERE id = %s AND project_id = %s""" %(usage_qty, id,ids[0]) 
                    cr.execute(sql)
                    print "SQL: ", sql
        
        return True
    
project_project()

class project_consumable(osv.osv):
    
   # def onchange_consumable(self, cr, uid, ids,product_id,context=None):
    
    def onchange_product(self, cr, uid, ids,product_id,context=None):
        val = {}
        domain = {}
        if product_id:
            product = self.pool.get('product.product').browse(cr,uid,product_id)
            val = {
                   'name':product.name,
                   'product_uom_id':product.uom_id.id,
                   }
            domain = {'product_uom_id':[('category_id','=',product.uom_id.category_id.id)]}
        return {'value':val,'domain':domain}
    
    
    def onchange_qty(self, cr, uid, ids, quantity, product_id, product_uom_id, context=None):
        #print "PRODUCT ID",product_id
        val = {}
        if product_id:
            product = self.pool.get('product.product').browse(cr, uid, product_id)
            uom_ref_id = self.pool.get('product.uom').search(cr, uid, [('category_id','=',product.uom_id.category_id.id),('uom_type','=','reference')])[0]
            uom_obj = self.pool.get('product.uom').browse(cr,uid, uom_ref_id)
        
            avail = (product.qty_available) * (1/product.uom_id.factor)
        
            if avail < quantity:
              raise osv.except_osv('Warning!', product.name + ' is out of stock! Available: ' + str(avail) + ' ' + uom_obj.name)
        
        return {'value':val}
        
    
    _name = 'project.consumable'
    _description = 'Consumables'
    _columns = {
                'name':fields.char('Desc', size=64),
                'product_id':fields.many2one('product.product', 'Consumable', required=True),
                'product_uom_id':fields.many2one('product.uom','UOM',required=True),
                'quantity':fields.float('Qty'),
                'remain_qty':fields.float('Remain'),
                'project_id':fields.many2one('project.project','Project',reequired=True),
                'location_id':fields.many2one('stock.location','Taken From', required=True),
                }
    
                
project_consumable()

class project_component(osv.osv):
    
   # def onchange_consumable(self, cr, uid, ids,product_id,context=None):
    
    def onchange_product(self, cr, uid, ids,product_id,context=None):
        val = {}
        domain = {}
        if product_id:
            product = self.pool.get('product.product').browse(cr,uid,product_id)
            val = {
                   'name':product.name,
                   'product_uom_id':product.uom_id.id,
                   }
            domain = {'product_uom_id':[('category_id','=',product.uom_id.category_id.id)]}
        return {'value':val,'domain':domain}
    
    
    def onchange_qty(self, cr, uid, ids, quantity, product_id, product_uom_id, context=None):
        #print "PRODUCT ID",product_id
        val = {}
        if product_id:
            product = self.pool.get('product.product').browse(cr, uid, product_id)
            uom_ref_id = self.pool.get('product.uom').search(cr, uid, [('category_id','=',product.uom_id.category_id.id),('uom_type','=','reference')])[0]
            uom_obj = self.pool.get('product.uom').browse(cr,uid, uom_ref_id)
        
            avail = (product.qty_available) * (1/product.uom_id.factor)
        
            if avail < quantity:
              raise osv.except_osv('Warning!', product.name + ' is out of stock! Available: ' + str(avail) + ' ' + uom_obj.name)
        
        return {'value':val}
        
    
    _name = 'project.component'
    _description = 'Component'
    _columns = {
                'name':fields.char('Desc', size=64),
                'product_id':fields.many2one('product.product', 'Component', required=True),
                'product_uom_id':fields.many2one('product.uom','UOM',required=True),
                'quantity':fields.float('Qty'),
                'remain_qty':fields.float('Remain'),
                'project_id':fields.many2one('project.project','Project',reequired=True),
                'location_id':fields.many2one('stock.location','Taken From', required=True),
                }
                    
project_component()